#Thorgate flavored Helm install image

The purpose of this repository is to build a minimal image with `helm` and `kubectl` installed
for the purpose of Kubernetes cluster integration.

* Helm version: `2.14.3`
* Kubectl versions:
  * `1.15.2`
  * `1.14.5`
  * `1.13.9`
  * `1.12.10`
